# Empezamos

**Processing** es un entorno de programación y un lenguaje de
programación creativos ideal para hacer artistadas y aprender a
programar casi sin darte cuenta.

**Processing** está basado en java, así que todo lo que aprendas
trabajando con **Processing** se puede aplicar a programar en **Java**

En **Processing** los programas se llaman `sketch` (bocetos) y se
guardan en una carpeta que se llama `sketchbook` (libro de bocetos) y
que si no la cambias en el menú *Preferencias* del IDE, estará en tu
directorio de documentos.

En preferencias también se puede poner el programa en español, se cambia
el idioma se cierra el programa y se vuelve a aabrir.

El nombre de un `sketch` se genera automaticamente a partir de la fecha,
pero es mejor guardarlo con algún nombre comprensible

En casi todos los lenguajes de programación lo primero que se hace, la
primera lección, es programar el “hola mundo”. Un programa que imprime
la frase “¡Hola Mundo\!”.

Si tenemos un `sketch` vacío y le damos al `play` veremos que aparece
una ventana con un cuadrado gris. Es el `canvas` (lienzo), este va a ser
el universo de nuestro programa en **Processing**, todo o casi todo va a
pasar en el lienzo.

En la parte de abajo de la ventana de programa está la consola de java
(a veces veremos cosas interesantes ahí)

Si cerramos el cuadrado gris podemos hacer nuestro primer programa en
**Processing**, como **Processing** es un programa gráfico nuestro hola
mundo será también gráfico.

El `canvas` (el lienzo de **Processing**) es una cuadricula de puntos
(muy pequeñitos, son pixeles). El origen (0, 0) está en la esquina
superior derecha,, cuanto mas alta sea la coordenada x más a la derecha
estará el punto, y cuanto más grande sea la y más abajo)

![Coordenadas en el *canvas*](src/img/canvas_coordinate_system.png)

**IMPORTANTE** Todas las ordenes de **Processing** acaban con un “;”
(con un punto y coma)

Vamos a probar la primera orden de **Procesing**: `size`

`size` define el tamaño del lienzo, así que `size(700, 200);` define un
lienzo de 700 puntos de ancho y 200 de alto.

Escribe la orden y dale al `play`, el lienzo debería haber cambiado al
nuevo tamaño.

Ahora queremos pintar un rectángulo en nuestro lienzo, eso se hace con
la orden `rect`, queremos un rectangulo que ocupe el cuadrante superiór
izquierdo del `canvas`, `rect` tiene cuatro parámetros, los dos primeros
son las coordenadas de la primera esquina que en este caso será el
origen (0, 0) y los dos segundos serán el ancho y el largo del
rectángulo (350, 100) en nuestro caso.

Asi que nuestro programa completo será:

``` {java}
size(700, 200);
rect(0, 0, 350, 100);
```

Teclea eso en la ventana de programas y dale al play

Deberías ver algo así:

![Hola Mundo](src/img/firstRect.png)

1.  prueba a pintar varios rectangulos y a cambiar el tamaño del canvas.
2.  prueba a pintar la cara de un robot con rectangulos
3.  Echa un ojo, pero solo un vistazo no te agobies, a la [Referencia de
    **Processing**](https://processing.org/reference/) puedes probar
    otras primitivas 2D en lugar de `rect`.

# Continuamos

Lo que estas usando hasta ahora son **funciones**, se llaman **funciones
primitivas** por que vienen de fábrica con el **Processing**, de momento
has usado `size` y `rect`.

Hay una funcion `ellipse` a la que se le pasan cuatro parámetros
(Xcentro, Ycentro, alto, ancho), huelga decir que si el alto y el ancho
son iguales pinta circulos.

Las funciones `rect` y `ellipse` tienen **modos** de funcionamiento. La
función `rect` por defecto tiene el modo `CORNER` (esquina), y la
función `ellipse` por defecto tiene el modo `CENTER` (centro) por eso
al rectangulo le pasamos de parámetro la esquina superior izquierda (y
despues ancho y alto) y a la elipse le pasamos el centro. Se pueden
cambiar: `rectMode(CENTER);` hara que todos los rectangulos que hagamos
después de esa linea se pinten en modo `CENTER` (pasando coordenadas del
centro no de la esquina). No hay premio por adivinar que hay una función
`ellipseMode`.

Hay también una función `line` que pinta una linea entre dos puntos,
tiene cuatro parámetros, `line(x1, y1, x2, y2);`

# Matices de gris

De momento vamos a seguir en blanco y negro.

El blanco y negro en **Processing** va desde el 0, que significa negro,
hasta el 255 que significa blanco. Estos valores tan arbitrarios vienen
del hecho de que en un octeto que son ocho bits se pueden representar
números desde el 0 al 255.

  - La función `background` establece el color del fondo. Asi que
    `background(255);` significa fondo blanco
  - La función `stroke` establece el color del borde, asi que
    `stroke(0);` significa borde negro
  - La funcion `fill` establece el color del relleno de las figuras
    `fill(150);` por ejemplo establece un relleno gris.

Aquí tienes un programa para inspirarte. Pruébalo en processing, todo lo
que se escriba en una linea a partir de dos barras `//` es un comentario
y no se ejecuta.

Todo lo que se escriba entre `/* y */` no importa en cuantas lineas es
un comentario y no se ejecuta

    /* Todo
    esto
    es un
    comentario  */

Es bueno comentar los programas para depues entenderlos, puedes probar a
comentar el siguiente programa, he comentado el final de todo

``` {java}
// Esto es un comentario, no hace nada, es para que tu lo leas


size(200, 200);
background(255);
rectMode(CENTER);
stroke(0);
fill(150);
rect(100, 100, 20, 100);

fill(255);
ellipse(100, 70, 60, 60);

fill(0);
ellipse(81, 70, 16, 32);
ellipse(119, 70, 16, 32);


// dibuja las piernas
stroke(0);                 // Trazo negro
line(90, 150, 80, 160);    // una pierna
line(110, 150, 120, 160);  // otra pierna

println("Me llamo Klaatu");  // imprime en la consola (abajo)
```

# Interacción

Todo esto está muy bien, pero ya tenemos programas de dibujo de sobras.
¿no podemos hacer nada más con **Processing**? ¿Como podemos añadir
movimiento a nuestros programas? ¿o permitir que interactuemos con
ellos?

Supongamos que le hemos hecho un platillo volante a Klaatu:

``` {java}
size(300,300);
background(255); //fondo blanco

stroke(0);
fill(245);
ellipse(150,135,70,60); //la cabina
stroke (0);
fill(200);
ellipse(150,160,150,40); //la base del platillo
```

¡Vamos a animarlo\!

En general todos los programas de **Processing** tienen dos partes
principales:

1.  Se prepara todo lo que sea necesario para nuestro programa: podría
    ser el canvas, colores figuras etc. etc.
2.  Se hace algo de forma repetitiva una y otra vez hasta que el
    programa se termina

Vamos que el esquema es como lo de hacer eliptica:

1.  Te pones calzado y ropa cómoda para hacer ejercicio
2.  Mueves una pierna y después la otra
3.  Repites el paso 2 si no has cumplido el objetivo del dia

Para hacer esto en **Processing** vamos a usar dos bloques de código
“especiales”, la verdad verdadera es que son funciones como las que
has usado hasta ahora, como por ejemplo `rect` o `ellipse`. Pero no
vamos a profundizar en eso de momento.

  - Un bloque de codigo en **Processing** se escribe entre dos llaves {
    }
  - Lo que se ejecuta **una sola vez** es el bloque de código dentro de
    la funcion `setup`, este es el primero de nuestros bloques de código
    “especiales”
  - Lo que se ejecuta **hasta siempre jamás** se escribe dentro de la
    función `draw`, y este es el segundo de nuestros dos bloques de
    código “especiales”

O sea:

``` {java}
{
  Esto sería un bloque de código
}
```

Los bloques de código se pueden anidar

``` {java}
{
  Esto es un bloque de código
  {
     esto es un bloque dentro del otro
  }
  Sique el bloque externo
}
```

Nuestros bloques de código especiales se escriben también de manera
especial:

``` {java}
void setup() {
    /*---------------------------
      Aquí va el código de setup
      ---------------------------*/

}

void draw(){
    /*---------------------------
      Aquí va el código de draw
      ---------------------------*/
}
```

Vale sigamos con nuestro platillo volante y lo reescribimos un poquillo:

    // Esta es la parte que solo se ejecuta una vez
    
    void setup() {
        size(300, 300);     // Tamaño del lienzo
        stroke(0);          // Vamos a usar borde negro siempre
    }
    
    //Esta es la parte que se ejecuta para siempre
    
    void draw(){
        background(255);    // fondo blanco, es importante ponerlo aquí y no en setup
    
        fill(245);
        ellipse(mouseX, (mouseY - 25), 70, 60);
        fill(200);
        ellipse(mouseX, mouseY, 150, 40);
    }

Ahora puedes probar este programa. Y magia potagia, tenemos interacción.

-----

**Nota:** Si estás pensando por que `background` no va en el `setup`,
este es el momento donde lo cambias y ves que pasa)

-----

Otra cosa que podemos hacer es imprimir en la consola los valores de
`mouseX`y `mouseY`, eso quizás te valga para hacer cálculos para los
dibujos

Bastaría añadir la linea `println(mouseX, mouseY);` al programa dentro
de la función `draw`, o si lo queremos más decorado:

    println( "(", mouseX, ", ", mouseY, ")");

y verás que abajo en la consola de java aparecen las coordenadas de la
posición del raton.

Además de `mouseX` y `mouseY` también tenemos `pmouseX` y `pmouseY`. Que
almacenan la posicion del ratón en el ciclo anterior.

Por ejemplo, podríamos hacer algo como esto:

    void setup() {
      size(200,200);
      background(255);
      smooth();
    }
    void draw() {
      stroke(0);
      line(pmouse X ,pmouse Y ,mouse X ,mouse Y );
    }

## Eventos

Un click de ratón o pulsar una tecla del teclado son eventos en
Processing. ¿Como se tratan los eventos? Pues muy fácil, tienen una
función asociada.

Cada vez que se hace click con cualquier botón del ratón **Processing**
intentará ejecutar la función `mousePressed`. Cada vez que apretamos una
tecla del teclado **Processing** intentará ejecutar la función
`keyPressed`

Para verlas en acción prueba el siguiente programa:

    void setup() {
      size(200,200);        // tamaño del canvas
      background(255);      // fondo blanco
    }
    
    void draw(){
      // no hace nada en este programa
      // pero es necesaria para que el programa esté en bucle
      // sin hacer nada y atienda los eventos
    }
    
    /* Definimos que pasará cada
       vez que se haga click con el ratón */
    void mousePressed() {
      stroke(0);                     // borde negro
      fill(175);                     // relleno gris
      rectMode(CENTER);              // dibujamos cuadrado con centro
      rect(mouseX, mouseY, 16, 16);  // en la posición del ratón
    }
    
    /*------------------------------------
      Definimos que pasará cada
      vez que se pulse una tecla del
      teclado
    -------------------------------------*/
    void keyPressed() {
      background(255);           // Pintamos toda la pantalla de blanco
    }                            // o sea borramos todo lo hecho

Fijate que aunque `draw` no hace nada en este programa lo tenemos que
poner de todas formas para que el programa se quede en el bucle y
atienda los eventos. Si no ponemos el `draw`, el programa simplemente
ejecutaría el `setup` y terminaría.

De hecho, **Processing** define dos modos de funcionamiento, `active`
(activo) y `static` (estático).

En las primeras lecciones usabamos el modo estático que solo permite
programas compuestoss de una lista de instrucciones y llamadas a otras
funciones.

En cuanto hacemos uso de `setup` o de `draw` pasamos al modo activo
(*active mode*) En ese modo `setup` se ocupa de la inicialización y
`draw` del ciclo. No es posible llamar a funciones en cualquier sitio.
Este programa por ejemplo nos dará un error y no se ejecutará, por que
invocamos la función `ellipse` fuera de `setup` y de `draw`.

``` {java}
ellipse(10, 10, 10, 10);

void setup(){
  println("Hola setup");
}

void draw(){
  println("Hola draw");
}
```

# Color

Bueno, ya hemos visto casi toda la presentación de **Processing** para
hacernos una idea de lo que es capaz de hacer, antes de empezar con la
parte interesante de verdad solo queda comentar una cosa: ¡colorines\!

En **Processing** podemos usar el sistema RGB para especificar colores
(RGB = *Red Green Blue*, o sea Rojo, Verde y Azul), cada posible color
se especifica como una combinación de Rojo Verde y Azul. Esto es la
verdad verdadera de como funciona la pantalla del ordenador, si la miras
con una lupa verás que cada pixel tiene tres puntos: rojo verde y azul.

Nota curiosa: Mezclar luz no es lo mismo que mezclar pinturas, así que
las reglas son distintas (por ejemplo el amarillo se consigue con Rojo y
Verde)

Escoger colores es muy fácil en **Processing** por qué en el menú de
herramientas tenemos un selector de colores, puedes escoger el color que
quieras y te apareceran los números RGB de ese color.

También hay un montón de páginas web dedicadas al tema, por ejemplo:

  - [Tabla de colores
    RGB](https://www.rapidtables.com/web/color/RGB_Color.html)
  - [ColourLovers](http://www.colourlovers.com/), un sitio donde la
    gente comparte paletas de colores para todo (la uso para diseños de
    paginas web)
  - [Paletton](http://paletton.com/#uid=32m0u0kllllaFw0g0qFqFg0w0aF), un
    sitio para generar paletas de colores para aplicaciones o
    presentaciones
  - [Coolors](https://coolors.co/090446-786f52-feb95f-f71735-c2095a),
    otro generador de paletas

También hay aplicaciones para el PC, por ejemplo si usas Linux: Agave

Bueno, lo dicho un color en RGB es la combinación de Rojo Verde y Azul,
así que vamos a tener tres valores (que van de 0 a 255)

  - ¿un relleno rojo brillante? pues escribimos `fill(255, 0, 0);`
  - ¿rojo oscuro? pues `fill(175, 0, 0);`
  - ¿rosa? entonces `fill(255, 200, 200);`

El selector de colores en el menú herramientas nos dice como hacerlo.
También podemos escribir los valores de los componentes y ver que valor
sale.

A veces los colores se escriben en Hexadecimal (base 16) por ejemplo
\#090446, se usa mucho el hexadecimal en computación por qué un octeto
(0-255) en hexadecimal se escribe con dos digitos así que el color
\#090446 equivale a RGB(9 , 4, 70), en el selector de colores se puede
escribir el número en hexadecimal abajo de todo y ya hace la traducción.

Para complicar las cosas, podemos añadir un cuarto valor a la
especificación de color (también de 0 a 255 ¿cómo no?). Eso sería el
canal *alpha* o transparencia. Si no lo escribes es lo mismo que poner
255 y significa opaco 100%, una valor por la mitad (127) significaría
50% de transparencia, etc, etc.

Con esto vale de momento para colores pero que sepas que se pueden hacer
mas cosas con `colorMode`, si quieres profundizar un poco más.

`colorMode` permite cambiar el sistema de colores de RGB a HUE, que
puede ser mas interesante para colorear algunas cosas.

`colorMode` también permite cambiar los numeros de referencia, si
quieres usar valores de 0 a 100 en lugar de 0 a 255 puedes escribir
`colorMode(RGB, 100);`. De hecho puedes poner un rango distinto para
cada color y para la transparencia: `colorMode(RGB,100,500,10,255);` si
es que tienes una buena razón para hacerlo, por qué lo normal es usar
los valores de 0-255 para todo como ves en las páginas web.

Y con esto acabamos la introducción a **Processing**, de momento solo
hemos llamado funciones y estaría bien que probaras más como:

  - Formas: `arc( )` , `curve( )`, `ellipse( )`, `line( )`, `point( )`,
    `quad( )`, `rect( )`, `triangle( )`
  - Colores: `background( )`, `colorMode( )`, `fill( )`, `noFill( )`,
    `noStroke( )`, `stroke( )`

En las siguientes chapas habrá que empezar con los otros dos aspectos de
la programación “Definir y usar variables” y el “Control del flujo de
programa” y con eso si que se pueden hacer cosas divertidas.

# Variables

Vamos a hablar de Variables. Son super útiles y es increible que hayamos
llegado tan lejos sin ellas.

¿Qué es una variable? Te la puedes imaginar como quieras, como una
cajita donde se pueden guardar valores para verlos más tarde, como una
variable de mates de las de toda la vida, como un papelito donde apuntas
un valor para mirarlo más tarde.

La verdad verdadera: Un ordenador tiene memoria, para hacer todas las
cosas que hace un ordenador necesita almacenar datos de forma más o
menos permanente (muy permanente el disco duro, poco permanente la
memoria RAM) Esa memoria, el ordenador la usa con direcciones, la
memoria de un ordenador es como un casillero con un numero enorme de
casillas y las direcciones de esas casillas son un numero grande. Para
los humanos no sería muy cómodo usar las direcciones de memoria, así que
en programación una posición de la memoria donde guardamos algún valor y
a la que asignamos un nombre, es una variable.

Para dejarlo más claro, vamos a revisar alguna variable que si que hemos
usado ya, por ejemplo `mouseX`. Eso es una variable, ni más ni menos.
**Processing** se encarga de actualizar la variable (cambiar el valor
almacenado en esa posición de la memoria) cada vez que movemos el ratón,
nosotros podemos mirar que valor tiene en cualquier momento usando su
nombre de variable en el programa.

`mouseX` es una variable “de sistema”, es el sistema (**Processing**)
quien se encarga de actualizar su valor. Nosotros no podemos darle el
valor que queramos a la variable.

Otras variables “de sistema” son `width`(ancho) y `height` (alto). Esas
variables quedan definidas al ejecutar el comando `size`.

``` {java}
// Esta es la parte que solo se ejecuta una vez

void setup() {
    size(1000, 600);     // Tamaño del lienzo
    stroke(0);          // Vamos a usar borde negro siempre
}

//Esta es la parte que se ejecuta para siempre

void draw(){
    background(255);    // fondo blanco, es importante ponerlo aquí y no en setup
                        // por que es importante queda de ejercicio :-D

    line(0, 0, width, height);            // dibuja diagonal
    line(0, height, width, 0);            // dibuja diagonal
    ellipse(width/2, height/2, 50, 50);   // circulo central
}
```

Fíjate en el programa de ejemplo y prueba a cambiar los valores de la
función `size` en la sección `setup`. Gracias a las variables el dibujo
siempre se adapta al tamaño del lienzo.

Lo mejor de todo es que no solo existen las variables “de sistema”.
Nosotros podemos crear variables y eso es super cómodo. Para crear
nuestras variables hay que tener en cuenta que **Processing** necesita
siempre que le digamos que tipo de valor vamos a guardar en la variable
(un número entero, un número real, una cadena de letras, etc.) y también
necesita saber que nombre le queremos dar a la variable. Y eso es todo:
**una variable tiene que tener un tipo y un nombre**.

Por ejemplo, imagínate que queremos guardar en un par de variables la
posición de un punto en el *canvas* de nuestro programa. Esas dos
variables son números enteros y eso se corresponde con el tipo `int` de
Processing. En el programa tendríamos que declarar las variables como:

``` {java}
int posicionX;
int posicionY;
```

Después de declaradas podemos guardar valores en ellas, por ejemplo:

``` {java}
posicionX = 350;
posicionY = 150;
```

En realidad, lo de crear una variable y guardar un valor en ella es tan
común que **Processing** permite escribir la declaración de una nueva
variable así:

``` {java}
int posicionX = 350;
int posicionY = 150;
```

Vamos a ver un ejemplo de como usar variables

Partimos de un programa sencillo que pinta un circulo en el medio del
lienzo:

``` {java}
void setup(){
  size(700, 300);
}

void draw(){
  ellipse(350, 150, 50, 50);
}
```

Emocionante ¿eh?

Vamos a definir una variable radio, para guardar el radio del circulo.

``` {java}
int radio = 50;

void setup(){
  size(700, 300);
}

void draw(){
  ellipse(350, 150, radio, radio);
}
```

Si queremos cambiar el valor del radio, ahora solo tenemos que cambiarlo
en la declaración de la variable, en este programa no importa mucho,
pero en un programa largo, muy largo, sería una gran ventaja

Vamos a definir un par de variables mas para la posición del círculo:

``` {java}
int radio = 50;
int posX = -25;
int posY = 150;

void setup(){
  size(700, 300);
}

void draw(){
  ellipse(posX, posY, radio, radio);
}
```

Una cosa reseñable de las variables es que varían (igual no te lo
esperabas)

Para cambiar el valor de una variable se usa una sentencia de
asignación, ahí van unas cuantas:

``` {java}
posX = 5;
posX = 5 + 4;
posX = 5 * posY;

posX = posX + 1;
```

La última igual te parece rara, pero se puede poner una variable en los
dos lados de una asignación. Esa sentencia significa aumentar el valor
de `posX` en uno.

Así que podemos hacer algo así:

``` {java}
int radio = 50;
int posX = 25;
int posY = 150;

void setup(){
  size(700, 300);
}

void draw(){
  background(125);
  ellipse(posX, posY, radio, radio);
  posX = posX + 1;
}
```

Eso ya mola mucho mas ¿no?

Como referencia los tipos primitivos de **Processing** (los tipos de
variable que vienen de fábrica) son:

  - **boolean**: Solo tiene dos valores true (verdadero) y false (falso)
  - **char**: una letra, ‘ a ’ ,‘b’ ,‘c’ , etc.
  - **byte**: a entero pequeño, solo va de –128 a 127
  - **short**: un entero algo más largo, –32768 a 32767
  - **int**: un entero, desde –2147483648 a 2147483647
  - **long**: un entero muy grande (el tamaño depende del sistema
    operativo)
  - **float**: un numero real con decimales, por ejemplo 3.141592
  - **double**: un número real con un montón de decimales, se usa si
    hacemos operaciones matemáticas muy precisas

También es interesante conocer las variables del sistema, son variable
que ya están predefinidas en **Processing**:

  - **width**: Es el ancho en pixels de nuestro canvas
  - **height**: Es el alto en pixels de nuestro lienzo
  - **frameCount**: Numero de tramas procesadas
  - **frameRate**: Numero de tramas por segundo que se están generando
  - **screen.width**: este es el ancho en pixels de nuestra pantalla
  - **screen.height**: altura en pixels de nuestra pantalla
  - **key**: La última tecla pulsada en el teclado
  - **keyCode**: El código numérico de la última tecla pulsada en el
    teclado
  - **keyPressed**: Vale True (verdadero) o false (falso)? Nos indica si
    una tecla está pulsada
  - **mousePressed**:True o false? Nos indica si hay algún botón del
    ratón pulsado
  - **mouseButton**: Que botón del ratón está pulsado? Left (izquierdo),
    right (derecho), o center (central)

# Azar

Hasta ahora hemos usado muchas funciones: `size`, `line`, `fill`,
`rect`… todo son funciones.

Incluso `setup` y `draw` son funciones.

Casi todas las funciones que hemos visto hasta ahora hacen que ocurra
algo en la pantalla de **Processing**. Pero las funciones también pueden
devolver resultados, de hecho se usan continuamente para eso.

La función `random` (literalmente aleatorio) nos devuelve un valor
`float` (un número real) aleatorio en el rango de numeros pasados como
parámetros.

``` {java}
float x = random(1, 100);  // una variable flotante entre 1 y 100
```

`random` devuelve siempre un `float` pero si necesitamos un entero
podemos usarlo en combinación con la función `int`.

``` {java}
int j = int( random(1, 100) ); // un entero entre 1 y 100
```
